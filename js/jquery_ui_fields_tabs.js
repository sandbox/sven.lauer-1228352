(function ($) {

  Drupal.behaviors.field_accordion_display = {
    attach: function (context, settings) {
      $('.jquery_ui_fields_tabs', context).tabs();
    }
  };

}(jQuery));