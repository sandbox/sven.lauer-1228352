(function ($) {

  Drupal.behaviors.field_accordion_display = {
    attach: function (context, settings) {
      $('.jquery_ui_fields_accordion', context).accordion({
       header : '.jqery_ui_fields_header',
       collapsible: true,
       active: false, 
       resetCss: true,
      });
    }
  };

}(jQuery));